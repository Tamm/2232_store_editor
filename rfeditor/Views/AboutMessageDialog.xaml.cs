﻿using System;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Reflection;
using System.Diagnostics;

namespace RFEditor.Views
{
    public partial class AboutMessageDialog : UserControl
    {
        public AboutMessageDialog()
        {
            DataContext = this;
            InitializeComponent();
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        public Version AssemblyVersion
        {
            get
            {
                return Assembly.GetEntryAssembly().GetName().Version;
            }
        }
    }
}
