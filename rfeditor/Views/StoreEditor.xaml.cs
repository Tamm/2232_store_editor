﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using RFEditor.Classes;
using RFEditor.Models;

namespace RFEditor.Views
{
    public partial class StoreEditor : UserControl
    {
        
        public StoreTreeViewModel ViewModel => DataContext as StoreTreeViewModel;

        public StoreEditor()
        {
            InitializeComponent();
        }

        private void DataGrid_GotFocus(object sender, RoutedEventArgs e)
        {
            // Lookup for the source to be DataGridCell
            if (e.OriginalSource.GetType() == typeof(DataGridCell))
            {
                // Starts the Edit on the row;
                DataGrid grd = (DataGrid)sender;
                grd.BeginEdit(e);
            }
        }
    }
}
