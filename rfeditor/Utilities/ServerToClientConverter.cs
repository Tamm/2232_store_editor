﻿using System;

namespace RFEditor.Utilities
{
    class ServerToClientConverter
    {
        public static UInt32 ServerToClient(char[] m_strCode)
        {
            if(m_strCode.Length != 7)
            {
                return 0;
            }

            byte Val0 = AlphaByte(m_strCode[0]);
            byte Val1 = AlphaByte(m_strCode[1]);
            byte Val2 = AlphaByte(m_strCode[2]);
            byte Val3 = AlphaByte(m_strCode[3]);
            byte Val4 = AlphaByte(m_strCode[4]);
            byte aVal5 = AlphaByte(m_strCode[5]);
            byte aVal6 = AlphaByte(m_strCode[6]);
            char Val5 = m_strCode[5];
            char Val6 = m_strCode[6];
            string Val56 = m_strCode[5].ToString() + m_strCode[6].ToString();
            int count;

            string value4 = "";
            string value1 = string.Format("{0}", (Val0 + Val1 + 1));
            string Number1;

            Number1 = value1;


            byte[] valuestest1 = StringToByteArray(Val56);
            byte[] ValTest1 = new byte[valuestest1.Length + 1];

            for (int i = 0; i < valuestest1.Length; i++)
            {
                ValTest1[i] = valuestest1[i];
            }


            if (m_strCode[0] != 'i' && m_strCode[2] != 'n')
            {

                if (m_strCode[0] == 'c') { value1 = (251 + Val2).ToString("X2"); }
                if (m_strCode[0] == 'e') { value1 = (128 + Val2).ToString("X2"); }
                if (m_strCode[0] == 'f') { value1 = (144 + Val2).ToString("X2"); }
                if (m_strCode[0] == 'g') { value1 = (160 + Val2).ToString("X2"); }
                if (m_strCode[0] == 'l') { value1 = (240 + Val2).ToString("X2"); }
                if (m_strCode[0] == 'u') { value1 = (180 + Val2).ToString("X2"); }
                if (m_strCode[0] == 'r' || m_strCode[0] == 'b') { value1 = (80 + Val2).ToString("X2"); }
                if (m_strCode[0] == 's') { value1 = (96 + Val2).ToString("X2"); }

                if (m_strCode[0] == 't') { value1 = (112 + BitConverter.ToInt16(ValTest1, 0)).ToString("X2"); }

            }
            else if (m_strCode[0] != 'i')
            {
                //for nukes that are in the trap.dat 
                {
                    value1 = (112 + Val2).ToString("X2");

                }
            }
            else
            { value1 = (192 + Val2).ToString("X2"); } //192
            //{ value1 = (Val2).ToString("X2"); } // for alpha

            value4 = (BitConverter.ToInt16(ValTest1, 0)).ToString("X2");
            //start of setting count
            if (m_strCode[2] == '0' && m_strCode[3] == '0' && m_strCode[4] == '0')
            { count = 1; }
            else if (m_strCode[3] == '0' && m_strCode[4] == '0')
            { count = 1; }
            else if (m_strCode[4] == '0')
            { count = 4; }
            else
            { count = 0; }
            //end of setting count
            switch (count)
            {

                case 4:
                    {
                        string myHex = string.Format("{0}{1}{2}00", value1, String.Format("{0:00}", Val3), value4);
                        UInt32 intAgain = UInt32.Parse(myHex, System.Globalization.NumberStyles.HexNumber);
                        return intAgain;
                    }
                default:
                    {
                        string myHex; //nukes go here
                        if (m_strCode[0] == 'i' || m_strCode[2] == 'n')
                        {
                            if (m_strCode[1] == 'k')
                            {
                                if (m_strCode[2] == 'x')
                                {
                                    myHex = string.Format("{0}{1}{3}{2}", value1, (Val3).ToString("X2"), value4, (Val4).ToString("X2"));
                                }
                                else if (m_strCode[2] == m_strCode[3])
                                {
                                    myHex = string.Format("{0}{1}{2}{3}", value1, (Val3).ToString("X2"), value4, (Val4).ToString("X2"));
                                }
                                else
                                {
                                    myHex = string.Format("{0}{1}{3}{2}", value1, (Val3).ToString("X2"), value4, (Val4).ToString("X2"));
                                }

                            }

                            else
                            {
                                myHex = string.Format("{0}{1}{2}{3}", value1, (Val3).ToString("X2"), (Val4).ToString("X2"), value4);
                            }

                        }
                        else
                        {
                            //tested on traps
                            if (m_strCode[1] == 'r')
                            {
                                myHex = string.Format("{0}{1}{2}{3}", value1, (Val3).ToString("X2"), (Val4).ToString("X2"), (Val4).ToString("X2"));
                            }
                            else if (m_strCode[1] == 'd' || m_strCode[1] == 'x')
                            {
                                myHex = string.Format("{0}{1}{2}{3}", value1, (Val3).ToString("X2"), (Val4).ToString("X2"), value4);
                            }
                            else
                            {
                                myHex = string.Format("{0}{1}{2}{3}", value1, (Val3).ToString("X2"), value4, (Val4).ToString("X2"));
                            }

                        }

                        UInt32 intAgain = UInt32.Parse(myHex, System.Globalization.NumberStyles.HexNumber);
                        return intAgain;
                    }

            }

        }

        public static byte AlphaByte(char m_strCode)
        {
            switch (m_strCode)
            {
                case 'a': { return (byte)0; }
                case 'b': { return (byte)1; }
                case 'c': { return (byte)2; }
                case 'd': { return (byte)3; }
                case 'e': { return (byte)4; }
                case 'f': { return (byte)5; }
                case 'g': { return (byte)6; }
                case 'h': { return (byte)7; }
                case 'i': { return (byte)8; }
                case 'j': { return (byte)9; }
                case 'k': { return (byte)10; }
                case 'l': { return (byte)11; }
                case 'm': { return (byte)12; }
                case 'n': { return (byte)13; }
                case 'o': { return (byte)14; }
                case 'p': { return (byte)15; }
                case 'q': { return (byte)16; }
                case 'r': { return (byte)17; }
                case 's': { return (byte)18; }
                case 't': { return (byte)19; }
                case 'u': { return (byte)20; }
                case 'v': { return (byte)21; }
                case 'w': { return (byte)22; }
                case 'x': { return (byte)23; }
                case 'y': { return (byte)24; }
                case 'z': { return (byte)25; }
                case '0': { return (byte)0; }
                case '1': { return (byte)1; }
                case '2': { return (byte)2; }
                case '3': { return (byte)3; }
                case '4': { return (byte)4; }
                case '5': { return (byte)5; }
                case '6': { return (byte)6; }
                case '7': { return (byte)7; }
                case '8': { return (byte)8; }
                case '9': { return (byte)9; }
                //Bullets
                case 'A': { return (byte)0; }
                case 'B': { return (byte)1; }
                case 'C': { return (byte)2; }
                case 'D': { return (byte)3; }
                case 'E': { return (byte)4; }
                case 'F': { return (byte)5; }
                case 'G': { return (byte)6; }
                case 'H': { return (byte)7; }
                case 'I': { return (byte)8; }
                case 'J': { return (byte)9; }
                case 'K': { return (byte)10; }
                case 'L': { return (byte)11; }
                case 'M': { return (byte)12; }
                case 'N': { return (byte)13; }
                case 'O': { return (byte)14; }
                default: return 0;
            }

        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}
