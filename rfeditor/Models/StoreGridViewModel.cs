﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using RFEditor.Utilities;

namespace RFEditor.Models
{
    public class StoreGridViewModel : INotifyPropertyChanged
    {
        private bool _isInit = true;
        private int _index;
        private bool _updated;
        private byte _type;
        private string _clientId;
        private string _itemId;
        private bool _error;
        private string _convertedId;

        public int Index
        {
            get { return _index; }
            set
            {
                if (_index == value) return;
                _index = value;
                OnPropertyChanged();
            }
        }

        public bool Updated
        {
            get { return _updated; }
            set
            {
                if (_updated == value) return;
                _updated = value;
                OnPropertyChanged();
            }
        }

        public byte Type
        {
            get { return _type; }
            set
            {
                if (_type == value) return;

                _type = value;

                if (!_isInit)
                {
                    Validate();
                    Updated = true;
                }
                OnPropertyChanged();
            }
        }

        public string ClientId
        {
            get { return _clientId; }
            set
            {
                if (_clientId == value) return;

                if (value == "0")
                {
                    _clientId = "";
                }
                else
                {
                    _clientId = value;
                }
                
                if (!_isInit)
                {
                    Validate();
                    Updated = true;
                }
                OnPropertyChanged();
            }
        }

        public string ItemId
        {
            get { return _itemId; }
            set
            {
                if (_itemId == value) return;

                if (value == "0")
                {
                    _itemId = "";
                }
                else
                {
                    _itemId = value;
                }

                if (!_isInit)
                {
                    Validate();
                    Updated = true;
                }
                OnPropertyChanged();
            }
        }

        public bool Error
        {
            get { return _error; }
            set
            {
                if (_error == value) return;
                _error = value;
                OnPropertyChanged();
            }
        }

        public string ConvertedId
        {
            get { return _convertedId; }
            set
            {
                if (_convertedId == value) return;

                if (value != "")
                {
                    char[] itemId = ItemId.ToCharArray();
                    _convertedId = ServerToClientConverter.ServerToClient(itemId).ToString("X8");

                    if (!_isInit)
                    {
                        if (ClientId == "")
                        {
                            ClientId = _convertedId;
                            Error = false;
                        }
                    }
                }
                else
                {
                    _convertedId = "";
                }

                OnPropertyChanged();
            }
        }

        public StoreGridViewModel(byte type, uint clientId, string itemId, int index)
        {

            Type = type;

            if(clientId == 0)
            {
                ClientId = "";
            } 
            else
            {
                ClientId = clientId.ToString("X8");
            }
            
            ItemId = itemId;
            Index = index;
            ConvertedId = ItemId;

            Validate();
            _isInit = false;
        }

        private void Validate()
        {
            bool validId = true;
            if (ItemId != "")
            {
                char[] itemId = ItemId.ToCharArray();
                uint id = ServerToClientConverter.ServerToClient(itemId);
                ConvertedId = id.ToString("X8");

                if (ConvertedId != ClientId)
                {
                    validId = false;
                } 
                else
                {
                    validId = true;
                }
            }

            if ((ItemId != "" && ClientId == "") || (ItemId == "" && ClientId != "") || !validId)
            {
                Error = true;
            }
            else
            {
                Error = false;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
