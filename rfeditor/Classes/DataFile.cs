﻿using RFEditor.Utilities;
using System;
using System.IO;

namespace RFEditor.Classes
{
    public struct ClientDataHeader
    {
        public uint nBlocks;
        public uint nColumns;
    }

    public struct ClientNDDataHeader
    {
        public uint nBlocks;
    }

    public struct ServerDataHeader
    {
        public uint nBlocks;
        public uint nColumns;
        public uint nSize;
    }

    public class DataFile
    {
        public const int SERVER_STORE_DATA = 0;
        public const int CLIENT_STORE_DATA = 1;
        public const int NDCLIENT_STORE_DATA = 2;

        public StoreListData storeListData;
        public StoreData storeData;
        public NDStoreData ndStoreData;


        public ClientDataHeader ReadClientDataHeader(BinaryReader br)
        {
            ClientDataHeader header = new ClientDataHeader();
            header.nBlocks = br.ReadUInt32();
            header.nColumns = br.ReadUInt32();

            return header;
        }

        public ClientNDDataHeader ReadClientNDDataHeader(BinaryReader br)
        {
            ClientNDDataHeader header = new ClientNDDataHeader();
            header.nBlocks = br.ReadUInt32();

            return header;
        }

        public ServerDataHeader ReadServerDataHeader(BinaryReader br)
        {
            ServerDataHeader header = new ServerDataHeader();
            header.nBlocks = br.ReadUInt32();
            header.nColumns = br.ReadUInt32();
            header.nSize = br.ReadUInt32();
            return header;
        }

        public void load(string fileName, int type)
        {
            BinaryReader br = new BinaryReader(File.OpenRead(fileName));
            int length = (int)br.BaseStream.Length;
            if (length > 0)
            {
                switch (type)
                {
                    case CLIENT_STORE_DATA:
                        storeData = new StoreData(ReadClientDataHeader(br));
                        BinaryUtils.UnSerialize(br, storeData.storeEntry);
                        break;
                    case NDCLIENT_STORE_DATA:
                        ndStoreData = new NDStoreData(ReadClientNDDataHeader(br));
                        BinaryUtils.UnSerialize(br, ndStoreData.ndStoreEntry);
                        break;
                    case SERVER_STORE_DATA:
                        storeListData = new StoreListData(ReadServerDataHeader(br));
                        BinaryUtils.UnSerialize(br, storeListData.storeListEntry);
                        break;
                }
            }
            br.Close();
        }     
    }
}
