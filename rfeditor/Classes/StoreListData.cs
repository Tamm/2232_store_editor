﻿using System;
using System.IO;

namespace RFEditor.Classes
{
    public class StoreListItemData
    {
        public byte[] itemId = new byte[64];
    }

    public class StoreListLimitedItemData
    {
        public byte[] m_strItemCode = new byte[64];
        public int m_nMaxCount;
    }

    public class NpcClassList
    {
        public int m_nNpc_Class;
    }

    public class StoreListEntry
    {
        public int m_dwIndex;
        public byte[] m_strCode = new byte[64];
        public byte[] m_strBinding_DummyName = new byte[64];
        public byte[] m_strStore_NPCcode = new byte[64];
        public byte[] m_strStore_NPCname = new byte[64];
        public byte[] m_strStore_MAPcode = new byte[64];
        public int m_nStore_trade;
        public int m_bSet_NPCangle;
        public int m_nStore_NPCangle;
        public NpcClassList[] m_nNpc_Class_List = new NpcClassList[10];
        public int m_nStore_LISTcount;
        public int m_nLimit_Listcount;
        public int m_nLimitItem_InitTime;
        public int m_nPriceSet;
        public int m_nItemUpCode;

        public StoreListItemData[] m_strItemlist = new StoreListItemData[200];
        public StoreListLimitedItemData[] m_limitedItemlist = new StoreListLimitedItemData[16];

        public StoreListEntry()
        {
            for (int i = 0; i < m_strItemlist.Length; i++)
            {
                m_strItemlist[i] = new StoreListItemData();
            }
            for (int i = 0; i < m_limitedItemlist.Length; i++)
            {
                m_limitedItemlist[i] = new StoreListLimitedItemData();
            }
            for (int i = 0; i < m_nNpc_Class_List.Length; i++)
            {
                m_nNpc_Class_List[i] = new NpcClassList();
            }
        }
    }

    public class StoreListData
    {
        public ServerDataHeader fileHeader;
        public StoreListEntry[] storeListEntry;
        
        public StoreListData(ServerDataHeader header)
        {
            fileHeader = header;
            storeListEntry = new StoreListEntry[fileHeader.nBlocks];
            for (int i = 0; i < storeListEntry.Length; i++)
            {
                storeListEntry[i] = new StoreListEntry();
            }
        }

        public StoreListEntry getEntryByIndex(int dwIndex)
        {
            return Array.Find(storeListEntry, entry => entry.m_dwIndex == dwIndex);
        }
    }
}
