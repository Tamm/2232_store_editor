﻿using System;
using System.IO;

namespace RFEditor.Classes
{
    public class NDStoreEntry
    {
        public uint dwIndex;
        public byte[] NPC = new byte[32];
        public byte[] Name = new byte[32];
        public uint descSize;
        public byte[] Description = new byte[0]; //null terminated size is descSize - null byte. Read dynamically right now

        public NDStoreEntry()
        {

        }
    }

    public class NDStoreData
    {
        public ClientNDDataHeader fileHeader;
        public NDStoreEntry[] ndStoreEntry;

        public NDStoreData(ClientNDDataHeader header)
        {
            fileHeader = header;
            ndStoreEntry = new NDStoreEntry[fileHeader.nBlocks];
            for (int i = 0; i < ndStoreEntry.Length; i++)
            {
                ndStoreEntry[i] = new NDStoreEntry();
            }
        }

        public NDStoreEntry getEntryByIndex(int dwIndex)
        {
            return Array.Find(ndStoreEntry, entry => entry.dwIndex == dwIndex);
        }
    }
}
